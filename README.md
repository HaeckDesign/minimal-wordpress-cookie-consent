=== Minimal WordPress Cookie Consent ===
Contributors: Matthew Haeck
Tags: cookie consent, cookie banner, minmal cookie consent, cookie banner, gdpr, tiny, minimal, cookie law, cookie notice
Requires at least: 3.1
Tested up to: 5.2.4
Stable tag: 1.0
License:
License URI:

The lightest WordPress cookie consent in the biz. Our Haeck Design Minimal Wordpress Cookie Consent manages all the basic requirements with none of the bells & whistles. We're obviously biased, but this is the best wordpress cookie plugin for decent devs looking to streamline their GDPR inclusion.


== Description ==

<h3>Key Features</h3>
<ul>
<li>Simple and lightweight</li>
<li>Modern and clean design</li>
<li>Built w/ UIKit</li>
</ul>

== Installation ==

<ol>
<li>Upload hd-cookie-consent folder to the `/wp-content/plugins/` directory.</li>
<li>Activate plugin through the 'Plugins' menu.</li>
<li>Customize the cookie.js if you want changes.</li>
<li>Feel free to compress once everything is set.</li>
</ol>

== Frequently Asked Questions ==

No FAQ's yet.

== Changelog ==

= 1.0 =
First version