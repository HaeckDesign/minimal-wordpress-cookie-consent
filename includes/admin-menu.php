<?php
// add_submenu_page('parent slug', 'page title', 'menu title', 'capabilty', 'slug url', 'output funtion');
function hdc_add_options_menu() {
	add_submenu_page('options-general.php', __('Cookie Settings', 'hd-cookie-consent'), 'Cookie', 'manage_options', 'hd-settings', 'hdc_options_page');
}
add_action('admin_menu', 'hdc_add_options_menu');
?>