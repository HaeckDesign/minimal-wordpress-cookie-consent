<?php
// Output function that displays the option page content
function hdc_options_page() {

	if ( !current_user_can( 'manage_options' ) ) {
		wp_die( 'Not quite fam.' );
	}

	$hdc_plugin_name = "HD Cookie Consent";

	ob_start(); ?>

	<div class="wrap theme-options-wrap">
	<h2><?php _e($hdc_plugin_name . ' Settings', 'hd-cookie-consent'); ?></h2>
	<br/>
	<h3><?php _e('Coded by Haeck Design', 'hd-cookie-consent'); ?></h3>
	<hr>
	<p>We like to keep things minimal, so you'll have to manually customize the script if you want to make adjustments. Have any questions? Shoot us a quick email.</p>
	<a href="https://haeckdesign.com/contact" target="_blank" rel="nofollow" title="Contact Haeck Design" class="button-primary">Contact Haeck Design</a>
	</div>

<?php

echo ob_get_clean();
}

?>