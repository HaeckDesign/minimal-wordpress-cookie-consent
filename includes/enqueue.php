<?php
/*******************************
* Script control
*******************************/

function hdc_load_options_page_scripts() {

	 wp_enqueue_style('options-styles', plugin_dir_url(__FILE__) . 'css/options-page.css');	 
}
add_action('admin_print_scripts', 'hdc_load_options_page_scripts');

function hdc_load_scripts() {
	
	 wp_enqueue_script('hd-scripts', plugin_dir_url(__FILE__) . 'js/cookies.js', null, null, true );
	 
}
add_action('wp_enqueue_scripts', 'hdc_load_scripts');
?>