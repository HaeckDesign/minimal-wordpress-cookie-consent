<?php
/*
* Plugin Name: HD Cookie Consent
* Plugin URI: https://matthewhaeck.com
* Description: A really simple, lightweight, responsive cookie consent plugin for WordPress.
* Version: 1.0
* Author: Matthew Haeck
* Author URI: https://matthewhaeck.com
* Text Domain: hd-cookie-consent
* Domain Path: /languages/
* License: GPL2
*/

/*  Copyright 2019  Matthew Haeck  (email : matthewhaeck@gmail.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


/*******************************
* Plugin Activation
*******************************/

function hdc_plugin_install() {
	// checks version of wordpress and deactives if lower than 3.1
	if (version_compare( get_bloginfo('version'), '3.1', '<')) {
		deactivate_plugins(basename(__FILE__)); // Deactivate my plugin
		wp_die( 'Your version of WordPress must be atleast 3.1 to use this plugin.' );
	}
}
register_activation_hook(__FILE__, 'hdc_plugin_install');

function hdc_load_plugin_textdomain() {
	load_plugin_textdomain( 'hd-cookie-consent', false, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'hdc_load_plugin_textdomain' );


/*******************************
* Includes
*******************************/

include_once( plugin_dir_path( __FILE__ ) . 'includes/enqueue.php');

if ( is_admin() ) {
	include_once( plugin_dir_path( __FILE__ ) . 'includes/admin-page.php'); 
	include_once( plugin_dir_path( __FILE__ ) . 'includes/admin-menu.php'); 
}


/*******************************
* Actions
*******************************/
add_action('wp_footer', 'hdcInsert');
?>